# Commandos

Como que o terraform irá conhecer as apis para qual ele vai distribuir os recursos?

Uma vez que os codigos estão feitos, é necessário que o terraform baixe o plugin necessário para a comunicação com o provider e defina o arquivo de estado para pode ele irá guardar o que ele fará. O comando que faz isso é o init

````bash
terraform init
````

o comando com plan faz um planejamento do que ele vai fazer, gerando uma saída do qeu acontecerá, sem executar. Dessa forma é possível fazer uma analise antes de aplciar

````bash
terraform plan
````

O comando apply e o destroy fazem exatamente o que dizem e precisa da configuração do usuário normalmente.

````bash
terraform apply
terraform destroy
````

Veja um fluxo para entender o caminho.

![Fluxo Avancado](../pics/fluxo2.png)

Uma vez que algo foi aplicado e o estado é preenchido, é possivel utilizar o terraform console para debbugar algumas coisas, ver os valores das variáveis, testar comandos, etc. Isso ajuda na hora de desenvolver.

````bash
terraform console
````