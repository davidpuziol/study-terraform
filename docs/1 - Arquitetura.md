# Architeura

Para entender o terraform, é preciso entender como ele funciona.

A grosso modo é assim:

Pensa no provider como `ONDE`

![Fluxo Basico](../pics/fluxo.png)

O estado (aquivo .tfstate) gerado ao fim da execução mostra o que o terraform jã criou lendo o arquivo descritivo.

Se o arquivo de estado não for encontrado ele irã achar que nada foi criado e provisionará tudo novamente. Se ele encontrar o arquivo de estado, ele vai comparar com o que já tem e vai aplicar a diferença no provider definido.

Por isso voce pode mexer a vontade no codigo, mudar várias coisas, que no fim ele vai saber o que precisa alterar e como fazer isso.

Uma coisa que precisa ficar claro é que arquivos declarados leem e criam recursos em um provider e cada provider tem a sua api que funciona também com alguma cli provavelmente.

> Uma cli da azure funciona para criar recursos na aws? Não. O terraform também não. A declaração é feita para um provider específico, por isso não da para reaproveitar o seu código criado para AWS para provisionar recursos no GCP, pois por baixo do pano ele está em contato direto com a CLI.

Verá mais pra frente que é possível declarar vários providers, mas os recursos são nomeados de acordo com o provider.

