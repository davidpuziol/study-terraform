# Instalação

> Antes de continuar o processo, leia tudo, pois existe um método mais fácil em  [Versões](#versões).

A instalação é muito simples, na verdade é fazer o download do binário do terraform e colocar no path para que possa ser encontrado o comando no terminal.

Terraform é opensource <https://github.com/hashicorp/terraform.git>

Você pode muito bem ir nas [releases](https://github.com/hashicorp/terraform/releases), baixar e extrair para uma pasta.

Ou clonar o projeto e instalar também.

````bash
git clone https://github.com/hashicorp/terraform.git
cd terraform
go install
````

Veja os vários métodos de instalação em <https://learn.hashicorp.com/tutorials/terraform/install-cli>

Vou deixar aqui somente a instalaçao para linux ubuntu para ser usado em scripts caso necessário, o resto veja na documentação

````bash
# Instalando pacotes de base para o script
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common
# Instalando a chave do repo da HashiCorp.
wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
# Verificando a chave
gpg --no-default-keyring \
    --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    --fingerprint
# Adicionando o repo oficial da hashicorp
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
# Atualizando os pacotes
sudo apt update
# Instalando 
sudo apt-get install terraform
# Para conferir
terraform --version
````

## Auto Complete do cli

Uma vez que o terraform tem uma cli nada como facilitar com um auto complete

Se vc usa o zsh irá ter o arquivo .zshrc em sua home ou se usa o bash terã o .bashrc

````bash
terraform -install-autocomplete
````

Abra novamente o seu terminal para que tudo funcione bem.

Agora com apertando o TAB vc auto completa os comandos.

## Versões

Geralmente em nos arquivos do terraform declaramos uma versão do binário do terraform para evitar eventuais problemas. Ou vc muda a versão do projeto para a que vc tem na sua máquina ou muda a versão do seu binário.

Para isso existe uma ferramenta interessante chamada [TFSwitch](https://github.com/warrensbox/terraform-switcher) que instala várias versões do terraform, `inclusive serve para a instalação inicial do terraform`

A `tfswitch` ferramenta de linha de comando permite alternar entre diferentes versões do terraform . Se você não tiver uma versão específica do terraform instalada, tfswitch permite que você baixe a versão desejada. A instalação é mínima e fácil. Depois de instalado, basta selecionar a versão desejada na lista suspensa e começar a usar o terraform.

Para instalar no linux

````bash
curl -L https://raw.githubusercontent.com/warrensbox/terraform-switcher/release/install.sh | sudo bash
````

Para instalar no mac desde que tenha o Homebrew já instalado.

````bash
brew install warrensbox/tap/tfswitch
````

O comando tfswitch já instala o terraform na versão que deseja

````bash
tfswitch
````

somente com o comando tfswitch no terminal sem nenhum parâmetro você já pode mudar para a versão desejada

````bash
❯tfswitch --help             

Usage: tfswitch [-hluv] [-b value] [-m value] [-p value] [-s value] [parameters ...]
 -b, --bin=value  Custom binary path. Ex: /Users/username/bin/terraform
 -h, --help       Displays help message
 -l, --list-all   List all versions of terraform - including beta and rc
 -m, --mirror=value
                  Install from a remote other than the default. Default:
                  https://releases.hashicorp.com/terraform
 -p, --latest-pre=value
                  Latest pre-release implicit version. Ex: tfswitch
                  --latest-pre 0.13 downloads 0.13.0-rc1 (latest)
 -s, --latest-stable=value
                  Latest implicit version. Ex: tfswitch --latest-stable 0.13
                  downloads 0.13.7 (latest)
 -u, --latest     Get latest stable version
 -v, --version    Displays the version of tfswitch
Supply the terraform version as an argument, or choose from a menu
david in ~ 
````

![tfswitch](../pics/tfswitch.png)