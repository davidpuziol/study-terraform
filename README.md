# Study Terraform

<img src="./pics/terraform.png" alt="drawing" width="300"/>

## O que é o Terraform?

Ferramenta para construir, alterar e criar versões de infraestrutura com segurança e eficiência. Isso se parece com uma infraestrutura como solução de código, porque Terraform é uma infraestrutura como solução de código. O Terraform nada mais é do que um binário que interpreta uma linguagem declarativa e cria recursos.

## Terraform vs Outros

Existem outras ferramentas também de declarar a infra estrurua

- [**Cloudformation**](https://aws.amazon.com/pt/cloudformation/) é a ferramenta da AWS exclusiva para provisionar recursos na AWS.

- [**Azure Resource Manager (ARM)**](https://learn.microsoft.com/en-us/azure/azure-resource-manager/management/overview) é a ferramenta da Azure exclusiva para provisionar recursos na Azure.

- [**Google Cloud Deployment Manager**](https://cloud.google.com/deployment-manager/docs) é a ferramenta do GCP exclusiva para provisionar recursos no GCP.

O Terraform consegue provisionar em qualquer cloud por isso podemos considerar um `Universal Infrastructure as Code` ou qualquer local que ofereça uma api que ele possa consumir. O terraform não trabalha somente com cloud e vamos entender isso mais pra frente.

Na mesma pegada ser universal temos o [**Ansible**](https://www.ansible.com/), [**Pulumi**](https://www.pulumi.com/).

>Apesar do ansible funcionar, não é a melhor escolha para criar recursos, mas para configurar sistemas.

O Pulumi vem crescendo, e veio com uma proposta de ter libs para muitas linguagens. O terraform tem a sua linguagem específica chamada `HCL`.

> O terraform por ser o primeiro, tem uma comunidade muito grande envolvida e uma documentação muito boa <https://www.terraform.io/docs>.


## Observação

Não existe voce provisionar um recurso em um provider, uma cloud por exemplo, sem conhecer sobre o recurso que vc deseja criar. Estude primeiro e entendar como as coisas funcionam sobre o recurso e depois coloque-o no terraform.

## Docs

[Arquitetura](./docs/1%20-%20Arquitetura.md)

[Instalação](./docs/1%20-%20Arquitetura.md)

[Comandos Básicos](./docs/1%20-%20Arquitetura.md)

[Providers](./docs/1%20-%20Arquitetura.md)

[State](./docs/1%20-%20Arquitetura.md)

[Variables e locals](./docs/1%20-%20Arquitetura.md)


# Extras

Procure encontrar um meio termo entre a modularização e o desempenho, pois quando se modulariza demais fica muito verboso trabalhar

Terraform 
Variáveis

Algumas anotações que valem a pena saber

É bom tipar todas as variáveis que serão usadas, para evitar problemas.

Em módulos é bom que o usuário passe todos os parâmetros evitando ao máximo utilizar valores default nas variáveis

Procure sempre descrever as variáveis

Em casos que os dados são sensíveis use o sensitive

Evite usar módulos para provider, pois dá muito problema de herança

Apesar do arquivo terraform.tfvars ser bom para guardar variável, é interessante manter só um arquivo variables.tf declarando as variáveis e seus tipos e descrições para facilitar a leitura. Utilize o tfvars e caso de teste.

Evite mudar o tfstate diretamente no código sem gerar um plano, dessa forma evitará erros

Procure fazer o validate das variáveis tanto quanto possível afim de evitar ter uma infra estrutura sem padrão de nomenclatura

Acostume-se a deixar no main.tf somente o provider e o backend e crie um arquivo terrafile.tf que irá de fato chamar os módulos e recursos.

Acostume-se a usar o comando terraform plan -out plano para criar o plano em um arquivo ao invés de usar somente terraform plan que fica em memória e usar o terraform apply plano para aplicar o plano ao inves de terraform apply que aplica o que estava em memória. Por padrão usa-se o nome plano como tfplan. É bom utilizar um padrão para poder ignorar os arquivos corretos no versionamento do código com o git.

Sempre faça um terraform validate no inicio do pipeline para não executar outras tarefas com erro básico no código

Sempre formate o código no padrão da hashcorp utilizando o comando terraform fmt

Não fuja do mindset normal das pessoas. Use comparação == ao invés de ! = sempre que possível.

 

Input Variables - Configuration Language | Terraform by HashiCorp 

 

 https://github.com/hashicorp/hcl